package com.application.tripatch.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.application.tripatch.R
import com.application.tripatch.model.News
import com.application.tripatch.model.Trip
import com.application.tripatch.utils.Tools
import com.balysv.materialripple.MaterialRippleLayout

    class AdapterMyTrips(ctx : Context, items : List<Trip>, layout_id : Int) : RecyclerView.Adapter<ViewHolderMyTrips>() {

    var items = items
    private val ctx: Context = ctx
    @LayoutRes
    private val layout_id: Int = layout_id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMyTrips {
        val vh: RecyclerView.ViewHolder
        val v = LayoutInflater.from(parent.context).inflate(layout_id, parent, false)
        vh = ViewHolderMyTrips(v)
        return vh
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderMyTrips, position: Int) {
            val n = items[position]
            holder.nameTrip.text = n.name
            holder.city.text = n.city
         //   Tools.displayImageOriginal(ctx, view.image, n.image)
            holder.myTripParent.setOnClickListener(View.OnClickListener { view ->
                if (mOnItemClickListener == null) return@OnClickListener
                mOnItemClickListener!!.onItemClick(view, items[position], position)
            })
    }

    private var mOnItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(view: View, obj: Trip, position: Int)
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.mOnItemClickListener = mItemClickListener
    }


}

class ViewHolderMyTrips(val view: View) : RecyclerView.ViewHolder(view){
    var image: ImageView = view.findViewById(R.id.image)
    var nameTrip: TextView = view.findViewById(R.id.name_trip)
    var city: TextView = view.findViewById(R.id.city)
    var myTripParent: MaterialRippleLayout = view.findViewById(R.id.my_trip_parent)
}