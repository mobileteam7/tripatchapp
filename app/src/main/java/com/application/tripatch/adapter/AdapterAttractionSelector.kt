package com.application.tripatch.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.application.tripatch.R
import com.application.tripatch.model.News
import com.application.tripatch.utils.Tools
import com.balysv.materialripple.MaterialRippleLayout

class AdapterAttractionSelector(ctx : Context, items : List<News>, layout_id : Int) : RecyclerView.Adapter<ViewHolderAttractionSelector>() {

    private val items = items
    private val ctx: Context = ctx
    @LayoutRes
    private val layout_id: Int = layout_id

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolderAttractionSelector, position: Int) {
        if (holder is ViewHolderAttractionSelector) {
            val view = holder

            val n = items[position]
            view.title.text = n.title
            view.subtitle.text = n.subtitle
            view.date.text = n.date
            Tools.displayImageOriginal(ctx, view.image, n.image)
            view.lyt_parent.setOnClickListener(View.OnClickListener { view ->
                if (mOnItemClickListener == null) return@OnClickListener
                mOnItemClickListener!!.onItemClick(view, items[position], position)
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderAttractionSelector {
        val vh: RecyclerView.ViewHolder
        val v = LayoutInflater.from(parent.context).inflate(layout_id, parent, false)
        vh = ViewHolderAttractionSelector(v)
        return vh
    }

    private var mOnItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(view: View, obj: News, position: Int)
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.mOnItemClickListener = mItemClickListener
    }

}

class ViewHolderAttractionSelector(val view: View) : RecyclerView.ViewHolder(view){
    var image: ImageView = view.findViewById(R.id.image)
    var title: TextView = view.findViewById(R.id.title)
    var subtitle: TextView = view.findViewById(R.id.subtitle)
    var date: TextView = view.findViewById(R.id.date)
    var lyt_parent: MaterialRippleLayout = view.findViewById(R.id.lyt_parent)
}