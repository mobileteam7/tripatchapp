package com.application.tripatch.data

import android.content.Context
import android.util.Log
import com.application.tripatch.R
import com.application.tripatch.model.News
import com.application.tripatch.model.Trip
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*
import com.google.firebase.firestore.DocumentSnapshot
import com.google.android.gms.tasks.Continuation


class DataProvider {

    companion object {

        private val r = Random()
        private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
        private var db = FirebaseFirestore.getInstance()

        fun getNewsData(ctx: Context, count: Int): List<News> {

            val items = ArrayList<News>()

            val images = getAllImages(ctx)
            val titles = getStringsMedium(ctx)
            val full_date = getFullDate(ctx)
            val cat = ctx.resources.getStringArray(R.array.news_category)

            for (i in 0 until count) {
                val obj = News()
                obj.image = images[getRandomIndex(images.size)]
                obj.title = titles[getRandomIndex(titles.size)]
                obj.subtitle = cat[getRandomIndex(cat.size)]
                obj.date = full_date[getRandomIndex(full_date.size)]
                items.add(obj)
            }
            return items
        }


        fun getMyTrips(count: Int) : Task<List<DocumentSnapshot>>? {
            var listTripsTask: Task<List<DocumentSnapshot>>? = null
            var tripsRef = ArrayList<DocumentReference>()

            var currentUser = mAuth.currentUser

            if (currentUser != null) {
                listTripsTask = db.collection("users").document(currentUser.uid).get()
                    .continueWithTask(Continuation<DocumentSnapshot, Task<List<DocumentSnapshot>>>
                    { task ->

                        val tasks = ArrayList<Task<DocumentSnapshot>>()
                        var userData = task.result as DocumentSnapshot
                        Log.d("DEB", userData.toString())
                        tripsRef = userData["trips"] as ArrayList<DocumentReference>
                        for (trip in tripsRef){
                            var t = db.document(trip.path).get()
                            tasks.add(t)
                        }
                        return@Continuation Tasks.whenAllSuccess(tasks)
                    })
            }
            return listTripsTask
        }


        private fun getAllImages(ctx: Context): List<Int> {
            val items = ArrayList<Int>()
            val drw_arr = ctx.resources.obtainTypedArray(R.array.all_images)
            for (i in 0 until drw_arr.length()) {
                items.add(drw_arr.getResourceId(i, -1))
            }
            items.shuffle()
            return items
        }

        private fun getStringsMedium(ctx: Context): List<String> {
            val items = ArrayList<String>()
            val name_arr = ctx.resources.getStringArray(R.array.strings_medium)
            for (s in name_arr) items.add(s)
            items.shuffle()
            return items
        }

        fun getFullDate(ctx: Context): List<String> {
            val items = ArrayList<String>()
            val name_arr = ctx.resources.getStringArray(R.array.full_date)
            for (s in name_arr) items.add(s)
            items.shuffle()
            return items
        }

        private fun getRandomIndex(max: Int): Int {
            return r.nextInt(max - 1)
        }
    }

}