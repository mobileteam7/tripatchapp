package com.application.tripatch.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.application.tripatch.R
import com.application.tripatch.activity.AttractionSelector
import com.application.tripatch.activity.CreateNewTrip
import com.application.tripatch.adapter.AdapterMyTrips
import com.application.tripatch.data.DataProvider
import com.application.tripatch.model.Trip
import kotlinx.android.synthetic.main.fragment_dashboard.*


class Dashboard : Fragment() {

    private var parent_view: View? = null
    private var mAdapter: AdapterMyTrips? = null
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_dashboard, container, false)
        parent_view = view.findViewById(android.R.id.content)
        initComponent(view)

        return view
    }


    override fun onStart() {
        super.onStart()

        updateAdapter()

        create_new_trip_button!!.setOnClickListener {
            var createNewTripIntent = Intent(activity, CreateNewTrip::class.java)
            createNewTripIntent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            startActivity(createNewTripIntent)
        }
    }

    private fun initComponent(view: View) {
        recyclerView = view.findViewById(R.id.recyclerViewMyTrips)
        recyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.setHasFixedSize(true)
    }

    private fun updateAdapter() {
        val items = ArrayList<Trip>()
        DataProvider.getMyTrips(20)?.addOnCompleteListener { listTripsTask ->

            var listTrips = listTripsTask.result
            for (trip in listTrips!!) {

                var newTrip = Trip()
                newTrip.name = trip["nameTrip"] as String
                newTrip.city = trip["city"] as String
                newTrip.dateFrom = trip["dateFrom"] as String
                newTrip.dateTo = trip["dateTo"] as String
                newTrip.ref = trip.reference
                items.add(newTrip)
            }

            mAdapter = activity?.let { AdapterMyTrips(it, items, R.layout.item_my_trip) }
            recyclerView.adapter = mAdapter
            mAdapter!!.setOnItemClickListener(object : AdapterMyTrips.OnItemClickListener {
                override fun onItemClick(view: View, obj: Trip, position: Int) {
                    parent_view = view.findViewById(android.R.id.content)
                    var attractionSelectorIntent = Intent(activity, AttractionSelector::class.java)
                    attractionSelectorIntent.putExtra("refToTrip", obj.ref.toString())
                    attractionSelectorIntent.putExtra("name", obj.name)
                    attractionSelectorIntent.putExtra("city", obj.city)
                    attractionSelectorIntent.putExtra("dateFrom", obj.dateFrom)
                    attractionSelectorIntent.putExtra("dateTo", obj.dateTo)
                    startActivity(attractionSelectorIntent)
                }
            })
        }
    }
}