package com.application.tripatch.model

import android.graphics.drawable.Drawable
import com.google.firebase.firestore.DocumentReference

class Trip(){
    var ref: DocumentReference? = null
    var name: String? = null
    var city: String? = null
    var dateFrom: String? = null
    var dateTo: String? = null
}