package com.application.tripatch.model

import android.graphics.drawable.Drawable

class News {

    var image: Int = 0
    var imageDrw: Drawable? = null
    var title: String? = null
    var subtitle: String? = null
    var date: String? = null
}
