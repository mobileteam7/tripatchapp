package com.application.tripatch.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.application.tripatch.R
import com.google.firebase.auth.FirebaseAuth

class Logout : AppCompatActivity() {

    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logout)


        mAuth.signOut()

        (findViewById<View>(R.id.sign_in_for_account)).setOnClickListener {
            var signUpIntent = Intent(applicationContext, Login::class.java)
            startActivity(signUpIntent)
            finish()
        }
    }

}
