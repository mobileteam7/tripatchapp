package com.application.tripatch.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.application.tripatch.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity(){

    private var progressBar: ProgressBar? = null
    private var fab: FloatingActionButton? = null
    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var currentUser: FirebaseUser? = null
    private var emailField: TextInputEditText? = null
    private var passwordField: TextInputEditText? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        progressBar = findViewById(R.id.progress_bar)
        fab = findViewById(R.id.login_fab)
        emailField = findViewById(R.id.emailField)
        passwordField = findViewById(R.id.passwordFiled)
        var parentView = findViewById<View>(android.R.id.content)

        //SignUp button
        (findViewById<View>(R.id.sign_up_for_account)).setOnClickListener {
            var signUpIntent = Intent(applicationContext, SignUp::class.java)
            startActivity(signUpIntent)
        }

        //SignIn button
        fab!!.setOnClickListener { loginAction(parentView) }
    }

    override fun onStart() {
        super.onStart()
        currentUser = mAuth.currentUser
        Log.d("User", currentUser.toString())
        if (currentUser!=null){
            var mainWindow = Intent(applicationContext, MainActivity::class.java)
            startActivity(mainWindow)
            finish()
        }
        progress_bar!!.visibility = View.GONE
        fab!!.alpha = 1f
        emailField?.setText("")
        passwordField?.setText("")
    }

    private fun loginAction(parentView:View) {
        progressBar!!.visibility = View.VISIBLE
        fab!!.alpha = 0f

        var email : String = emailField?.text.toString()
        var password : String = passwordField?.text.toString()

        if(email!=null && email!="" && password!=null && password!=""){
            mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful){
                        var mainWindow = Intent(applicationContext, MainActivity::class.java)
                        startActivity(mainWindow)
                        finish()
                    }else{
                        progress_bar!!.visibility = View.GONE
                        fab!!.alpha = 1f
                        Snackbar.make(parentView, "Wrong email or password", Snackbar.LENGTH_SHORT).show()
                    }
                }
        }
    }
}