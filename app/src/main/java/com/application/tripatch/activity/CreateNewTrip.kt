package com.application.tripatch.activity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.application.tripatch.R
import com.application.tripatch.utils.Tools
import com.google.android.material.textfield.TextInputEditText
import java.util.*
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.google.firebase.firestore.FirebaseFirestore
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import kotlin.collections.HashMap


class CreateNewTrip : AppCompatActivity() {

    private var db = FirebaseFirestore.getInstance()
    private var currentUser: FirebaseUser? = null
    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    var dialogDatePickerTurnOn : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_new_trip)

        initToolbar()
        initComponent()
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
     //   Tools.setSystemBarColor(this, android.R.color.black)
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun initComponent() {
        (findViewById<TextInputEditText>(R.id.date_from)).setOnTouchListener { view: View, motionEvent: MotionEvent ->
            if (!dialogDatePickerTurnOn){
                dialogDatePickerTurnOn = true
                dialogDatePickerLight(R.id.date_from)
            }
            return@setOnTouchListener true
        }
        (findViewById<TextInputEditText>(R.id.date_to)).setOnTouchListener { view: View, motionEvent: MotionEvent ->
            if (!dialogDatePickerTurnOn){
                dialogDatePickerTurnOn = true
                dialogDatePickerLight(R.id.date_to)
            }
            return@setOnTouchListener true
        }
        (findViewById<View>(R.id.create_new_trip_button_accept)).setOnClickListener { view: View ->
            createTripInDatabase()
        }

    }

    private fun dialogDatePickerLight(id : Int) {
        val cur_calender = Calendar.getInstance()

        val datePicker = DatePickerDialog.newInstance(
            { view, year, monthOfYear, dayOfMonth ->
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val date_ship_millis = calendar.timeInMillis
                (findViewById<TextView>(id)).text = Tools.getFormattedDateSimple(
                    date_ship_millis
                )
                dialogDatePickerTurnOn = false
            },
            cur_calender.get(Calendar.YEAR),
            cur_calender.get(Calendar.MONTH),
            cur_calender.get(Calendar.DAY_OF_MONTH)
        )
        //set dark light

        datePicker.isThemeDark = false
        datePicker.accentColor = resources.getColor(R.color.colorPrimary)
        datePicker.minDate = cur_calender
        datePicker.show(fragmentManager, "Datepickerdialog")
    }

    private fun createTripInDatabase(){
        var nameTrip = findViewById<TextInputEditText>(R.id.name_trip).text.toString()
        var city = findViewById<TextInputEditText>(R.id.city).text.toString()
        var dateFrom = findViewById<TextInputEditText>(R.id.date_from).text.toString()
        var dateTo = findViewById<TextInputEditText>(R.id.date_to).text.toString()

        currentUser = mAuth.currentUser
        if(currentUser!=null){
            var guid = currentUser!!.uid

            val trip : HashMap<String, Any> = HashMap()
            trip["nameTrip"] = nameTrip
            trip["city"] = city
            trip["dateFrom"] = dateFrom
            trip["dateTo"] = dateTo


            db.collection("users").document(guid).get()
                .addOnSuccessListener(this) {
                    var userData = it.data as HashMap<String, Any>

                    db.collection("trips").add(trip)
                        .addOnSuccessListener {
                            var tripDocRef = it
                            var userTrips = userData["trips"] as MutableList<DocumentReference>
                            userTrips.add(tripDocRef)

                            db.collection("users").document(guid).update("trips", userTrips)

                            var attractionSelectorIntent = Intent(applicationContext, AttractionSelector::class.java)
                            startActivity(attractionSelectorIntent)
                        }
                }
        }
    }
}
