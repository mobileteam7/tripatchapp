package com.application.tripatch.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.application.tripatch.R
import com.application.tripatch.adapter.AdapterAttractionSelector
import com.application.tripatch.data.DataProvider
import com.application.tripatch.model.News
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_atraction_selector.*

class AttractionSelector : AppCompatActivity() {


    private lateinit var parent_view: View

   // private var recyclerView: RecyclerView? = null
    private var mAdapter: AdapterAttractionSelector? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_atraction_selector)
        parent_view = findViewById(android.R.id.content)

        initToolbar()
        initComponent()
    }


    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Edit trip"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
      //  Tools.setSystemBarColor(this, R.color.grey_1000)
    }

    private fun initComponent() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        val items = DataProvider.getNewsData(this, 20)

        //set data and list adapter
        mAdapter = AdapterAttractionSelector(this, items, R.layout.item_attraction)
        recyclerView.adapter = mAdapter

        // on item list clicked
        mAdapter!!.setOnItemClickListener(object : AdapterAttractionSelector.OnItemClickListener {
            override fun onItemClick(view: View, obj: News, position: Int) {
                Snackbar.make(parent_view, "Item " + obj.title + " clicked", Snackbar.LENGTH_SHORT)
                    .show()
            }
        })

    }


//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menuInflater.inflate(R.menu.menu_search_setting, menu)
//      //  Tools.changeMenuIconColor(menu, Color.WHITE)
//        return true
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else {
            Toast.makeText(applicationContext, item.title, Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }
}
