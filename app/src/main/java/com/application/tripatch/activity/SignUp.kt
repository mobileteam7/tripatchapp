package com.application.tripatch.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.application.tripatch.R
import com.application.tripatch.utils.Tools
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login.*
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.google.firebase.firestore.DocumentReference
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.HashMap


class SignUp : AppCompatActivity() {

    private var signup_progressBar: ProgressBar? = null
    private var signup_fab: FloatingActionButton? = null
    private var db = FirebaseFirestore.getInstance()
    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        signup_progressBar = findViewById(R.id.signup_progress_bar)
        signup_fab = findViewById(R.id.signup_fab)

        initComponent()
    }

    private fun initComponent() {

        (findViewById<View>(R.id.signup_fab)).setOnClickListener {
            signUp()
        }
    }

    fun signUp(){
        signup_progressBar!!.visibility = View.VISIBLE
        signup_fab!!.alpha = 0f

        var email  = findViewById<TextInputEditText>(R.id.email_signUp).text.toString()
        var password = findViewById<TextInputEditText>(R.id.password_signUp).text.toString()
        var confirmPassword = findViewById<TextInputEditText>(R.id.confirm_password_signUp).text.toString()

        if(password!=confirmPassword){
            Toast.makeText(
                applicationContext,
                "Password not correct",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                val user = mAuth.currentUser
                if (user != null) {
                    Log.d("DEB", user.toString())
                    createUserInDatabase(user)
                }
                var signUpIntent = Intent(applicationContext, Login::class.java)
                startActivity(signUpIntent)
                finish()
            } else {
                signup_progressBar!!.visibility = View.GONE
                signup_fab!!.alpha = 1f
                Toast.makeText(applicationContext, "Authentication failed.",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun createUserInDatabase(user : FirebaseUser){
        val userData : HashMap<String, Any> = HashMap()
        var trip = emptyList<DocumentReference>()
        var date : Date = Date()
        userData.put("trips", trip)
        userData.put("createdData", date.time)
        userData.put("createdBy", "system")
        userData.put("lastModifiedDate", date.time)
        userData.put("lastModifiedBy", "system")

        db.collection("users").document(user.uid).set(userData)
    }

}
